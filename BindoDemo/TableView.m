//
//  TableView.m
//  MultipeerDemo
//
//  Created by Matt Chang on 9/11/15.
//  Copyright (c) 2015 Accuvally Inc. All rights reserved.
//

#import "TableView.h"
#import "UIView+draggable.h"

@interface TableView () {
    UIBezierPath *_bezierPath;
    CGFloat _startX;
    CGFloat _startY;
    CGFloat _sideTableLength;
    CGFloat _sideSeatLength;
    CGFloat _seatWidth;
    NSUInteger _seatCount;
    CGFloat _seatDistance;
    CGFloat _angle;
    
    UILabel *_numberLabel;
}

- (void)drawSquare;
- (void)drawRectangle;
- (void)drawRound;
- (void)setupChairs;
- (void)tapEvent;
- (CGPoint)nextSquareSeatPoint:(CGPoint)point andIndex:(NSUInteger)index andoOrientation:(NSUInteger)orientation;
- (CGPoint)nextRectangleSeatPoint:(CGPoint)point andIndex:(NSUInteger)index andoOrientation:(NSUInteger)orientation;

@end

CGFloat const kTableWidth = 50;
CGFloat const _seatDistance = 10;

@implementation TableView

- (id)initWithTable:(Table *)table {
    self = [super initWithFrame:CGRectMake(0, 0, 100, 100)];
    
    if (self) {
        self.table = table;
        self.isSelected = NO;
        self.backgroundColor = [UIColor clearColor];
        
        if (self.table.count > 4) {
            _seatDistance = 5;
        } else {
            _seatDistance = 10;
        }
        
        _sideTableLength = kTableWidth/sqrt(2);
        _sideSeatLength = _seatDistance/sqrt(2);
        if (self.table.isOpposing) {
            _seatCount = self.table.count/2;
        } else {
            _seatCount = (self.table.count-2)/2;
        }
        if (self.table.count%2 ==1) {
            _seatCount++;
        }
        if (_seatCount < 1 || (self.table.tableType == TableTypeSquare && self.table.count < 5)) {
            // for 2 seats
            _seatCount = 1;
        }
        _seatWidth = (kTableWidth-(_seatCount+1)*_seatDistance)/_seatCount;
        
        _angle = 2*M_PI/self.table.count;
        
        [self enableDragging];
        __weak TableView *weakSelf = self;
        self.draggingStartedBlock = ^{
            __strong TableView *strongSelf = weakSelf;
            [strongSelf.superview bringSubviewToFront:strongSelf];
        };
        self.draggingEndedBlock = ^{
            __strong TableView *strongSelf = weakSelf;
            strongSelf.table.position = NSStringFromCGPoint(strongSelf.center);
        };
        
        _numberLabel = [[UILabel alloc] initWithFrame:CGRectMake(35, 35, 30, 30)];
        _numberLabel.textColor = [UIColor blackColor];
        _numberLabel.textAlignment = NSTextAlignmentCenter;
        _numberLabel.text = self.table.name;
        [self addSubview:_numberLabel];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapEvent)];
        [self addGestureRecognizer:tap];
    }
    
    return self;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    if (self.table.tableType == TableTypeSquare) {
        if (self.table.count > 4) {
            [self drawRectangle];
        } else {
            [self drawSquare];
        }
    } else if (self.table.tableType == TableTypeRound) {
        [self drawRound];
    }
    
    UIColor *color = [UIColor colorWithWhite:1 alpha:1];
    
    
    [color set];
    [_bezierPath stroke];
    
    if (self.isSelected) {
        color = [UIColor colorWithRed:0.0 green:0.6 blue:0.99 alpha:1];
    } else {
        color = [UIColor colorWithWhite:1 alpha:1];
    }
    [color setFill];
    [_bezierPath fill];
    
    [self setupChairs];
    
}

- (void)setNeedsDisplay {
    [super setNeedsDisplay];
    _numberLabel.text = self.table.name;
}

#pragma mark - Private

- (void)drawSquare {
    _bezierPath = [UIBezierPath bezierPath];
    
    _bezierPath.lineWidth = 0.7;
    
    _startX = CGRectGetWidth(self.frame)/2;
    _startY = (CGRectGetHeight(self.frame)-2*kTableWidth/sqrt(2))/2;
    
    [_bezierPath moveToPoint:CGPointMake(_startX, _startY)];
    [_bezierPath addLineToPoint:CGPointMake(_startX+_sideTableLength, _startY+_sideTableLength)];
    [_bezierPath addLineToPoint:CGPointMake(_startX, _startY+2*_sideTableLength)];
    [_bezierPath addLineToPoint:CGPointMake(_startX-_sideTableLength, _startY+_sideTableLength)];
    [_bezierPath addLineToPoint:CGPointMake(_startX, _startY)];
    
}

- (void)drawRectangle {
    _bezierPath = [UIBezierPath bezierPath];
    
    _bezierPath.lineWidth = 0.8;
    
    _startX = (CGRectGetHeight(self.frame)-kTableWidth)/2;
    _startY = (CGRectGetHeight(self.frame)-kTableWidth/2)/2;
    
    [_bezierPath moveToPoint:CGPointMake(_startX, _startY)];
    [_bezierPath addLineToPoint:CGPointMake(_startX+kTableWidth, _startY)];
    [_bezierPath addLineToPoint:CGPointMake(_startX+kTableWidth, _startY+kTableWidth/2)];
    [_bezierPath addLineToPoint:CGPointMake(_startX, _startY+kTableWidth/2)];
    [_bezierPath addLineToPoint:CGPointMake(_startX, _startY)];
    
}

- (void)drawRound {
    _bezierPath = [UIBezierPath bezierPath];
    
    _bezierPath.lineWidth = 0.8;
    
    [_bezierPath addArcWithCenter:CGPointMake(CGRectGetWidth(self.frame)/2, CGRectGetHeight(self.frame)/2) radius:kTableWidth/2 startAngle:0 endAngle:2*M_PI clockwise:YES];
    
    _startX = CGRectGetWidth(self.frame)/2;
    _startY = (CGRectGetHeight(self.frame)-kTableWidth)/2;
}

- (void)setupChairs {
    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
    bezierPath.lineWidth = 0.5;
    
    if (self.table.tableType == TableTypeSquare) {
        if (self.table.count > 4) {
            for (int i = 0; i < self.table.count; i++) {
                CGPoint startPoint;
                
                if (self.table.isOpposing) {
                    NSUInteger orientation;
                    if (i%2 == 0) {
                        startPoint = CGPointMake(_startX+i*(_seatDistance+_seatWidth)/2+_seatDistance, _startY);
                        orientation = 0;
                    } else {
                        startPoint = CGPointMake(_startX+kTableWidth-(i-1)*(_seatDistance+_seatWidth)/2-_seatDistance, _startY+kTableWidth/2);
                        orientation = 2;
                    }
                    
                    [bezierPath moveToPoint:startPoint];
                    CGPoint point1 = [self nextRectangleSeatPoint:startPoint andIndex:1 andoOrientation:orientation];
                    [bezierPath addLineToPoint:point1];
                    CGPoint point2 = [self nextRectangleSeatPoint:point1 andIndex:2 andoOrientation:orientation];
                    [bezierPath addLineToPoint:point2];
                    CGPoint point3 = [self nextRectangleSeatPoint:point2 andIndex:3 andoOrientation:orientation];
                    [bezierPath addLineToPoint:point3];
                    
                    
                } else {
                    if (i == 0) {
                        startPoint = CGPointMake(_startX+_seatDistance, _startY);
                    } else if (i == 1) {
                        startPoint = CGPointMake(_startX+kTableWidth, _startY+(kTableWidth/2-_seatWidth)/2);
                    } else if (i == 2) {
                        startPoint = CGPointMake(_startX+kTableWidth-_seatDistance, _startY+kTableWidth/2);
                    } else if (i == 3) {
                        startPoint = CGPointMake(_startX, _startY+(kTableWidth/2+_seatWidth)/2);
                    } else {
                        if (i%2 == 0) {
                            startPoint = CGPointMake(_startX+(i-2)*(_seatDistance+_seatWidth)/2+_seatDistance, _startY);
                        } else {
                            startPoint = CGPointMake(_startX+kTableWidth-(i-3)*(_seatDistance+_seatWidth)/2-_seatDistance, _startY+kTableWidth/2);
                        }
                    }
                    
                    [bezierPath moveToPoint:startPoint];
                    
                    if (i > 3) {
                        if (i%2 == 0) {
                            CGPoint point1 = [self nextRectangleSeatPoint:startPoint andIndex:1 andoOrientation:0];
                            [bezierPath addLineToPoint:point1];
                            CGPoint point2 = [self nextRectangleSeatPoint:point1 andIndex:2 andoOrientation:0];
                            [bezierPath addLineToPoint:point2];
                            CGPoint point3 = [self nextRectangleSeatPoint:point2 andIndex:3 andoOrientation:0];
                            [bezierPath addLineToPoint:point3];
                        } else {
                            CGPoint point1 = [self nextRectangleSeatPoint:startPoint andIndex:1 andoOrientation:2];
                            [bezierPath addLineToPoint:point1];
                            CGPoint point2 = [self nextRectangleSeatPoint:point1 andIndex:2 andoOrientation:2];
                            [bezierPath addLineToPoint:point2];
                            CGPoint point3 = [self nextRectangleSeatPoint:point2 andIndex:3 andoOrientation:2];
                            [bezierPath addLineToPoint:point3];
                        }
                    } else {
                        CGPoint point1 = [self nextRectangleSeatPoint:startPoint andIndex:1 andoOrientation:i%4];
                        [bezierPath addLineToPoint:point1];
                        CGPoint point2 = [self nextRectangleSeatPoint:point1 andIndex:2 andoOrientation:i%4];
                        [bezierPath addLineToPoint:point2];
                        CGPoint point3 = [self nextRectangleSeatPoint:point2 andIndex:3 andoOrientation:i%4];
                        [bezierPath addLineToPoint:point3];
                    }
                }
                
            }
        } else {
            for (int i = 0; i < self.table.count; i++) {
                CGPoint startPoint;
                switch (i%4) {
                    case 0:
                        startPoint = CGPointMake(_startX+_sideSeatLength, _startY+_sideSeatLength);
                        break;
                    case 1:
                        startPoint = CGPointMake(_startX+_sideTableLength-_sideSeatLength, _startY+_sideSeatLength+_sideTableLength);
                        break;
                    case 2:
                        startPoint = CGPointMake(_startX-_sideSeatLength, _startY+2*_sideTableLength-_sideSeatLength);
                        break;
                    case 3:
                        startPoint = CGPointMake(_startX-_sideTableLength+_sideSeatLength, _startY+_sideTableLength-_sideSeatLength);
                        break;
                    default:
                        break;
                }
                
                if (self.table.isOpposing && i ==1) {
                    startPoint = CGPointMake(_startX-_sideSeatLength, _startY+2*_sideTableLength-_sideSeatLength);
                    [bezierPath moveToPoint:startPoint];
                    CGPoint point1 = [self nextSquareSeatPoint:startPoint andIndex:1 andoOrientation:2];
                    [bezierPath addLineToPoint:point1];
                    CGPoint point2 = [self nextSquareSeatPoint:point1 andIndex:2 andoOrientation:2];
                    [bezierPath addLineToPoint:point2];
                    CGPoint point3 = [self nextSquareSeatPoint:point2 andIndex:3 andoOrientation:2];
                    [bezierPath addLineToPoint:point3];
                } else {
                    [bezierPath moveToPoint:startPoint];
                    CGPoint point1 = [self nextSquareSeatPoint:startPoint andIndex:1 andoOrientation:i%4];
                    [bezierPath addLineToPoint:point1];
                    CGPoint point2 = [self nextSquareSeatPoint:point1 andIndex:2 andoOrientation:i%4];
                    [bezierPath addLineToPoint:point2];
                    CGPoint point3 = [self nextSquareSeatPoint:point2 andIndex:3 andoOrientation:i%4];
                    [bezierPath addLineToPoint:point3];
                }
            }
            
        }
    } else if (self.table.tableType == TableTypeRound) {
        double theta = 2*M_PI/self.table.count;
        for (int i = 1; i <= self.table.count; i++) {
            CGPoint startPoint = CGPointMake((kTableWidth+20)/2*cos(theta*i)+CGRectGetWidth(self.frame)/2, (kTableWidth+20)/2*sin(theta*i)+CGRectGetHeight(self.frame)/2);
            UIBezierPath *path = [UIBezierPath bezierPath];
            [path addArcWithCenter:startPoint radius:5 startAngle:0 endAngle:2*M_PI clockwise:YES];
            path.lineWidth = 0.5;
            UIColor *color = [UIColor colorWithWhite:1 alpha:1];
            [color set];
            [path stroke];
        }
    }
    
    
    UIColor *color = [UIColor colorWithWhite:1 alpha:1];
    [color set];
    [bezierPath stroke];
}

- (void)tapEvent {
    self.tapHanlder();
}

- (CGPoint)nextSquareSeatPoint:(CGPoint)point andIndex:(NSUInteger)index andoOrientation:(NSUInteger)orientation {
    CGPoint resultPoint;
    switch (index) {
        case 1:
            switch (orientation) {
                case 0:
                    resultPoint = CGPointMake(point.x+_sideSeatLength, point.y-_sideSeatLength);
                    break;
                case 1:
                    resultPoint = CGPointMake(point.x+_sideSeatLength, point.y+_sideSeatLength);
                    break;
                case 2:
                    resultPoint = CGPointMake(point.x-_sideSeatLength, point.y+_sideSeatLength);
                    break;
                case 3:
                    resultPoint = CGPointMake(point.x-_sideSeatLength, point.y-_sideSeatLength);
                    break;
                default:
                    break;
            }
            
            break;
        case 2:
            switch (orientation) {
                case 0:
                    resultPoint = CGPointMake(point.x+_seatWidth/sqrt(2), point.y+_seatWidth/sqrt(2));
                    break;
                case 1:
                    resultPoint = CGPointMake(point.x-_seatWidth/sqrt(2), point.y+_seatWidth/sqrt(2));
                    break;
                case 2:
                    resultPoint = CGPointMake(point.x-_seatWidth/sqrt(2), point.y-_seatWidth/sqrt(2));
                    break;
                case 3:
                    resultPoint = CGPointMake(point.x+_seatWidth/sqrt(2), point.y-_seatWidth/sqrt(2));
                    break;
                default:
                    break;
            }
            
            break;
        case 3:
            switch (orientation) {
                case 0:
                    resultPoint = CGPointMake(point.x-_sideSeatLength, point.y+_sideSeatLength);
                    break;
                case 1:
                    resultPoint = CGPointMake(point.x-_sideSeatLength, point.y-_sideSeatLength);
                    break;
                case 2:
                    resultPoint = CGPointMake(point.x+_sideSeatLength, point.y-_sideSeatLength);
                    break;
                case 3:
                    resultPoint = CGPointMake(point.x+_sideSeatLength, point.y+_sideSeatLength);
                    break;
                default:
                    break;
            }
            
            break;
        default:
            break;
    }
    
    
    return resultPoint;
}

- (CGPoint)nextRectangleSeatPoint:(CGPoint)point andIndex:(NSUInteger)index andoOrientation:(NSUInteger)orientation {
    CGPoint resultPoint;
    switch (index) {
        case 1:
            switch (orientation) {
                case 0:
                    resultPoint = CGPointMake(point.x, point.y-_seatDistance);
                    break;
                case 1:
                    resultPoint = CGPointMake(point.x+_seatDistance, point.y);
                    break;
                case 2:
                    resultPoint = CGPointMake(point.x, point.y+_seatDistance);
                    break;
                case 3:
                    resultPoint = CGPointMake(point.x-_seatDistance, point.y);
                    break;
                default:
                    break;
            }
            
            break;
        case 2:
            switch (orientation) {
                case 0:
                    resultPoint = CGPointMake(point.x+_seatWidth, point.y);
                    break;
                case 1:
                    resultPoint = CGPointMake(point.x, point.y+_seatWidth);
                    break;
                case 2:
                    resultPoint = CGPointMake(point.x-_seatWidth, point.y);
                    break;
                case 3:
                    resultPoint = CGPointMake(point.x, point.y-_seatWidth);
                    break;
                default:
                    break;
            }
            
            break;
        case 3:
            switch (orientation) {
                case 0:
                    resultPoint = CGPointMake(point.x, point.y+_seatDistance);
                    break;
                case 1:
                    resultPoint = CGPointMake(point.x-_seatDistance, point.y);
                    break;
                case 2:
                    resultPoint = CGPointMake(point.x, point.y-_seatDistance);
                    break;
                case 3:
                    resultPoint = CGPointMake(point.x+_seatDistance, point.y);
                    break;
                default:
                    break;
            }
            
            break;
        default:
            break;
    }
    
    
    return resultPoint;
}

@end
