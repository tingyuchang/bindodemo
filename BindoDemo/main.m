//
//  main.m
//  BindoDemo
//
//  Created by Matt Chang on 9/17/15.
//  Copyright (c) 2015 Matt Chang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
