//
//  ViewModel.m
//  BindoDemo
//
//  Created by Matt Chang on 9/17/15.
//  Copyright (c) 2015 Matt Chang. All rights reserved.
//

#import "ViewModel.h"
#import <MagicalRecord/MagicalRecord.h>
#import "SaveRecord.h"
#import "ConnectionManager.h"

@interface ViewModel ()

- (void)save:(id)subscriber;
- (void)reloadData;

@end

@implementation ViewModel

- (id)init {
    self = [super init];
    
    if (self) {
        [self reloadData];
        if (self.rooms.count > 0) {
            self.currentRoom = [self.rooms objectAtIndex:0];
            [self syncData];
        }
        self.insertTableSubject = [RACSubject new];
    }
    
    return self;
}

- (RACSignal *)insertRoom:(NSString *)name {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        Room *room = [Room MR_createEntity];
        room.name = name;
        [self save:subscriber];
        return nil;
    }];
}

- (RACSignal *)insertTable:(NSDictionary *)dict{
    @weakify(self);
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        /*
         @{
         @"type": @(self.newTableType),
         @"name": @(_newTableNumber),
         @"seats": @(_newTableSeats),
         @"opposing": @(_newTableIsOpposing),
         @"position": NSStringFromCGPoint(_roomView.center)
         }
         */
        @strongify(self);
        Table *table = [Table MR_createEntity];
        table.type = dict[@"type"];
        table.name = dict[@"name"];
        table.seats = dict[@"seats"];
        table.opposing = dict[@"opposing"];
        table.position = dict[@"position"];
        [self.currentRoom addTablesObject:table];
        [subscriber sendNext:table];
        [self.insertTableSubject sendNext:@(self.currentRoom.tables.count)];
        return nil;
    }];
}

- (RACSignal *)saveSetting {
    @weakify(self);
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self);
        [self save:subscriber];
        return nil;
    }];
}

- (RACSignal *)removeCurrentRoom {
    @weakify(self);
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self);
        [self.currentRoom MR_deleteEntity];
        [self save:subscriber];
        return nil;
    }];
}

- (RACSignal *)removeTable:(Table *)table {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [table MR_deleteEntity];
        [subscriber sendCompleted];
        [self save:subscriber];
        return nil;
    }];
}

- (void)selectedCurrentRoom:(NSUInteger)index {
    self.currentRoom = [self.rooms objectAtIndex:index];
}

- (void)syncData {
    NSMutableArray *rooms = [NSMutableArray new];
    for (Room *savedRoom in self.rooms) {
        NSArray *keys = [[[savedRoom entity] attributesByName] allKeys];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[savedRoom dictionaryWithValuesForKeys:keys]];
        NSMutableArray *tables = [NSMutableArray new];
        for (Table *savedTable in savedRoom.tables) {
            NSArray *keys = [[[savedTable entity] attributesByName] allKeys];
            NSDictionary *dict = [savedTable dictionaryWithValuesForKeys:keys];
            [tables addObject:dict];
        }
        [dict setObject:tables forKey:@"tables"];
        [rooms addObject:dict];
    }
    
    NSMutableDictionary *savedData = [NSMutableDictionary new];
    [savedData setObject:rooms forKey:@"rooms"];
    
    SaveRecord *saveRecord = [SaveRecord MR_findFirstOrderedByAttribute:@"timestamp" ascending:NO];
    [savedData setObject:saveRecord.timestamp forKey:@"timestamp"];

    NSData *dataToSend = [NSKeyedArchiver archivedDataWithRootObject:savedData];
    NSArray *allPeers = [ConnectionManager sharedInstance].session.connectedPeers;
    NSError *error;
    
    [[ConnectionManager sharedInstance].session sendData:dataToSend toPeers:allPeers withMode:MCSessionSendDataReliable error:&error];
    
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    }
}

#pragma mark - Private

- (void)reloadData {
    self.rooms = [Room MR_findAll];
}

- (void)save:(id)subscriber {
    @weakify(self);
    SaveRecord *record = [SaveRecord MR_createEntity];
    record.timestamp = [NSDate date];
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        if (success) {
            @strongify(self);
            [self reloadData];
            [subscriber sendCompleted];
        } else {
            [subscriber sendError:nil];
        }
    }];
}

@end
