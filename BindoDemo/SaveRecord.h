//
//  SaveRecord.h
//  BindoDemo
//
//  Created by Matt Chang on 9/17/15.
//  Copyright (c) 2015 Matt Chang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SaveRecord : NSManagedObject

@property (nonatomic, retain) NSDate * timestamp;

@end
