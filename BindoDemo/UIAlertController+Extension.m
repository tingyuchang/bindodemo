//
//  UIAlertController+Extension.m
//  BindoDemo
//
//  Created by Matt Chang on 9/17/15.
//  Copyright (c) 2015 Matt Chang. All rights reserved.
//

#import "UIAlertController+Extension.h"

@implementation UIAlertController (Extension)


+ (UIAlertController *)alertTitle:(NSString *)title message:(NSString *)message {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    return alertController;
}

+ (UIAlertController *)actionSheetTitle:(NSString *)title message:(NSString *)message {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleActionSheet];
    
    return alertController;
}

- (void)addActionTitle:(NSString *)title style:(UIAlertActionStyle)style handler:(void (^)(UIAlertAction *action))handler {
    UIAlertAction *action = [UIAlertAction actionWithTitle:title style:style handler:^(UIAlertAction *action) {
        if (handler) {
            handler(action);
        }
    }];
    
    [self addAction:action];
}


@end
