//
//  TableSettingView.h
//  BindoDemo
//
//  Created by Matt Chang on 9/19/15.
//  Copyright (c) 2015 Matt Chang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Table.h"
#import <ReactiveCocoa.h>
#import <RACEXTScope.h>

@interface TableSettingView : UIView

@property (assign) TableType type;
@property (assign) BOOL isOpposing;
@property (assign) BOOL isEditing;
@property (strong, nonatomic) UIButton *doneButton;
@property (strong, nonatomic) UIButton *cancelButton;

@property (strong, nonatomic) UIButton *reloadButton;
@property (strong, nonatomic) UIButton *duplicateButton;
@property (strong, nonatomic) UIButton *removeButton;

- (void)resetSettingView:(NSUInteger)numberIndex;
- (NSDictionary *)getTableInfo;
- (NSDictionary *)duplicateTable;
- (void)changeToEditMode:(NSDictionary *)dict;
@end
