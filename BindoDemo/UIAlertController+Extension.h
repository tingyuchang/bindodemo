//
//  UIAlertController+Extension.h
//  BindoDemo
//
//  Created by Matt Chang on 9/17/15.
//  Copyright (c) 2015 Matt Chang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertController (Extension)

+ (UIAlertController *)alertTitle:(NSString *)title message:(NSString *)message;

+ (UIAlertController *)actionSheetTitle:(NSString *)title message:(NSString *)message;

- (void)addActionTitle:(NSString *)title style:(UIAlertActionStyle)style handler:(void (^)(UIAlertAction *action))handler;

@end
