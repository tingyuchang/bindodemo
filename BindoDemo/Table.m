//
//  Table.m
//  BindoDemo
//
//  Created by Matt Chang on 9/17/15.
//  Copyright (c) 2015 Matt Chang. All rights reserved.
//

#import "Table.h"
#import "Room.h"


@implementation Table

@dynamic name;
@dynamic opposing;
@dynamic position;
@dynamic seats;
@dynamic type;
@dynamic room;

- (TableType)tableType {
    return [self.type integerValue];
}

- (NSUInteger)count {
    return [self.seats integerValue];
}

- (CGPoint)center {
    return CGPointFromString(self.position);
}

- (BOOL)isOpposing {
    if ([self.opposing integerValue] == 1) {
        return YES;
    }
    return NO;
}

@end
