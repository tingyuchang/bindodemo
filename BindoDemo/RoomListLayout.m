//
//  RoomListLayout.m
//  BindoDemo
//
//  Created by Matt Chang on 9/17/15.
//  Copyright (c) 2015 Matt Chang. All rights reserved.
//

#import "RoomListLayout.h"

@implementation RoomListLayout

- (id)init {
    self = [super init];
    
    if (self) {
        self.itemSize = CGSizeMake(100, 70);
        self.scrollDirection = UICollectionViewScrollDirectionVertical;
        // 邊緣
        self.sectionInset = UIEdgeInsetsMake(0, 0, 0, 10);
        self.minimumLineSpacing = 0;
        self.minimumInteritemSpacing = 0.0;
    }
    
    return self;
}

@end
