//
//  ViewController.m
//  BindoDemo
//
//  Created by Matt Chang on 9/17/15.
//  Copyright (c) 2015 Matt Chang. All rights reserved.
//

#import "ViewController.h"
#import "RoomListLayout.h"
#import "RoomCell.h"
#import "Room.h"
#import "UIAlertController+Extension.h"
#import "Table.h"
#import "TableView.h"
#import "TableSettingView.h"

@interface ViewController ()<UICollectionViewDataSource, UICollectionViewDelegate> {
    UICollectionView *_roomlistView;
    
    UIView *_roomView;
    TableSettingView *_tableSettingView;
    
    NSInteger _newTableNumber;
    NSInteger _newTableSeats;
    BOOL _newTableIsOpposing;
    
    TableView *_selectedTableView;
    
}

@property (assign) TableType newTableType;

- (void)decorateView;
- (void)decorateTableSettingView;
- (void)addRoom;
- (RACCommand *)insertTableToRoomCommand;
- (RACCommand *)duplicateTableToRoomCommand;
- (RACCommand *)removeTableFromRoomCommand;
- (void)reloadRoomView;
- (void)reloadTableSettingView;
- (void)selectRoomAtIndex:(NSUInteger)index;
- (void)selectTableViewToEdit;
- (void)cancelEditing;

@end

@implementation ViewController

- (id)init {
    self = [super init];
    
    if (self) {
        self.viewModel = [ViewModel new];
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:1 green:0.2 blue:0.2 alpha:1];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor], NSFontAttributeName: [UIFont systemFontOfSize:30]};
    self.title = @"Restaurant Layout";
    self.view.backgroundColor = [UIColor blackColor];
    
    [self decorateView];
    [self decorateTableSettingView];
    
    if (self.viewModel.rooms.count > 0) {
        [_roomlistView reloadData];
        [_roomlistView layoutIfNeeded];
        [self selectRoomAtIndex:0];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private

- (void)decorateView {
    _roomlistView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame)-230, 70) collectionViewLayout:[RoomListLayout new]];
    _roomlistView.backgroundColor = [UIColor clearColor];
    _roomlistView.dataSource = self;
    _roomlistView.delegate = self;
    [_roomlistView registerClass:[RoomCell class] forCellWithReuseIdentifier:@"ROOMCELL"];
    [self.view addSubview:_roomlistView];
    
    UIView *line2 = [[UIView alloc] initWithFrame:CGRectMake(0, 70, CGRectGetWidth(self.view.frame)-300, 0.7)];
    line2.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:line2];
    
    _roomView = [[UIView alloc] initWithFrame:CGRectMake(0, 70.7, CGRectGetWidth(self.view.frame)-300, CGRectGetHeight(self.view.frame)-70.7-64-60)];
    _roomView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:_roomView];
    
    UIButton *addRoomButton = [UIButton buttonWithType:UIButtonTypeCustom];
    addRoomButton.frame = CGRectMake(CGRectGetWidth(self.view.frame)-370, 0, 70, 70);
    [addRoomButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [addRoomButton addTarget:self action:@selector(addRoom) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:addRoomButton];
    
    UILabel *addLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 70, 70)];
    addLabel.text = @"+";
    addLabel.font = [UIFont systemFontOfSize:40];
    addLabel.textColor = [UIColor whiteColor];
    addLabel.textAlignment = NSTextAlignmentCenter;
    [addRoomButton addSubview:addLabel];
    
    UIButton *saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    saveButton.frame = CGRectMake(50, CGRectGetHeight(self.view.frame)-55-64, 200, 45);
    saveButton.layer.cornerRadius = 5.0f;
    saveButton.backgroundColor = [UIColor greenColor];
    [saveButton setTitle:@"Save Room" forState:UIControlStateNormal];
    [saveButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:saveButton];
    
    saveButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        [[self.viewModel saveSetting] subscribeCompleted:^{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congradulations!" message:@"save success" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }];
        return [RACSignal empty];
    }];
    
    UIButton *deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    deleteButton.frame = CGRectMake(350, CGRectGetHeight(self.view.frame)-55-64, 200, 45);
    deleteButton.layer.cornerRadius = 5.0f;
    deleteButton.backgroundColor = [UIColor redColor];
    [deleteButton setTitle:@"Delete Room" forState:UIControlStateNormal];
    [deleteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:deleteButton];
    
    deleteButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {[[self.viewModel saveSetting] subscribeCompleted:^{
        [[self.viewModel removeCurrentRoom] subscribeCompleted:^{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congradulations!" message:@"delete success" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [alert.rac_buttonClickedSignal subscribeNext:^(id x) {
                [_roomlistView reloadData];
                [_roomlistView layoutIfNeeded];
                [self selectRoomAtIndex:0];
            }];
        }];
    }];
        return [RACSignal empty];
    }];
}

- (void)decorateTableSettingView {
    _tableSettingView = [[TableSettingView alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.view.frame)-300, 0, 300, CGRectGetHeight(self.view.frame))];
    _tableSettingView.backgroundColor = [UIColor colorWithWhite:0.13 alpha:1];
    _tableSettingView.doneButton.rac_command = [self insertTableToRoomCommand];
    [self.view addSubview:_tableSettingView];

    [self.viewModel.insertTableSubject subscribeNext:^(NSNumber *number) {
        [_tableSettingView resetSettingView:[number integerValue]+1];
    }];
}

- (void)addRoom {
    UIAlertController *alertController = [UIAlertController alertTitle:@"Please enter room name" message:nil];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"Room Name";
     }];
    [alertController addActionTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
    __weak UIAlertController *weakAlert = alertController;
    @weakify(self);
    [alertController addActionTitle:@"Confirm" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        @strongify(self);
        __strong UIAlertController *strongAlert = weakAlert;
        UITextField *textField = [strongAlert.textFields objectAtIndex:0];
        [[self.viewModel insertRoom:textField.text] subscribeError:^(NSError *error) {
        
        } completed:^{
            [_roomlistView reloadData];
            [_roomlistView  layoutIfNeeded];
            [self selectRoomAtIndex:self.viewModel.rooms.count-1];
        }];
    }];
    [self presentViewController:alertController animated:YES completion:nil];

}

- (RACCommand *)insertTableToRoomCommand {
    return [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        [[self.viewModel insertTable:[_tableSettingView getTableInfo]] subscribeNext:^(Table *x) {
            TableView *tableView = [[TableView alloc] initWithTable:x];
            tableView.center = CGPointFromString(x.position);
            @weakify(self);
            @weakify(tableView);
            tableView.tapHanlder = ^(){
                @strongify(self);
                @strongify(tableView);
                if (_selectedTableView == tableView) {
                    _selectedTableView.isSelected = NO;
                    [_selectedTableView setNeedsDisplay];
                } else {
                
                }
                tableView.isSelected = !tableView.isSelected;
                [tableView setNeedsDisplay];
                if (_selectedTableView.isSelected == YES) {
                    [self selectTableViewToEdit];
                    _selectedTableView = tableView;
                } else {
                    [self reloadTableSettingView];
                    _selectedTableView = nil;
                }
            };
            [_roomView addSubview:tableView];
        }];
        return [RACSignal empty];
    }];
}

- (RACCommand *)duplicateTableToRoomCommand {
    return [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        [[self.viewModel insertTable:[_tableSettingView duplicateTable]] subscribeNext:^(Table *x) {
            TableView *tableView = [[TableView alloc] initWithTable:x];
            tableView.center = CGPointFromString(x.position);
            @weakify(self);
            @weakify(tableView);
            tableView.tapHanlder = ^(){
                @strongify(self);
                @strongify(tableView);
                if (_selectedTableView == tableView) {
                    _selectedTableView.isSelected = NO;
                    [_selectedTableView setNeedsDisplay];
                }
                tableView.isSelected = !tableView.isSelected;
                [tableView setNeedsDisplay];
                if (_selectedTableView.isSelected == YES) {
                    [self selectTableViewToEdit];
                    _selectedTableView = tableView;
                } else {
                    [self reloadTableSettingView];
                    _selectedTableView = nil;
                }
            };
            [_roomView addSubview:tableView];
            _selectedTableView.isSelected = NO;
            [_selectedTableView setNeedsDisplay];
            _selectedTableView = nil;
        }];
        return [RACSignal empty];
    }];
}

- (RACCommand *)removeTableFromRoomCommand {
    return [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        [_selectedTableView removeFromSuperview];
        [[self.viewModel removeTable:_selectedTableView.table] subscribeCompleted:^{
            _selectedTableView = nil;
        }];
        return [RACSignal empty];
    }];
}

- (void)reloadRoomView {
    [_roomView removeFromSuperview];
    _roomView = [[UIView alloc] initWithFrame:CGRectMake(0, 70.7, CGRectGetWidth(self.view.frame)-300, CGRectGetHeight(self.view.frame)-70.7-64-60)];
    _roomView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:_roomView];
    
    for (Table *table in self.viewModel.currentRoom.tables) {
        TableView *tableView = [[TableView alloc] initWithTable:table];
        tableView.center = CGPointFromString(table.position);
        @weakify(self);
        @weakify(tableView);
        tableView.tapHanlder = ^(){
            @strongify(self);
            @strongify(tableView);
            _selectedTableView.isSelected = NO;
            [_selectedTableView setNeedsDisplay];
            if (_selectedTableView == tableView) {
                [self reloadTableSettingView];
                _selectedTableView = nil;
            } else {
                tableView.isSelected = YES;
                [tableView setNeedsDisplay];
                _selectedTableView = tableView;
                [self selectTableViewToEdit];

            }
        };
        [_roomView addSubview:tableView];
    }
    
}

- (void)reloadTableSettingView {
    [_tableSettingView resetSettingView:[self.viewModel.currentRoom.tables count]+1];
}

- (void)selectRoomAtIndex:(NSUInteger)index {
    RoomCell *cell = (RoomCell *)[_roomlistView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
    cell.selectedMarkView.hidden = NO;
    [self.viewModel selectedCurrentRoom:index];
    [self reloadRoomView];
    [self reloadTableSettingView];
}

- (void)selectTableViewToEdit{
    /*
     @{
     @"type": @(self.type),
     @"name": _numberLabel.text,
     @"seats": @(_seatsLabel.text.integerValue),
     @"opposing": @(self.isOpposing),
     }
     */
    [_tableSettingView changeToEditMode:@{
                                          @"type": _selectedTableView.table.type,
                                          @"name": _selectedTableView.table.name,
                                          @"seats": _selectedTableView.table.seats,
                                          @"opposing": _selectedTableView.table.opposing
                                          }];
    _tableSettingView.reloadButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        NSDictionary *dict = [_tableSettingView getTableInfo];
        _selectedTableView.table.type = dict[@"type"];
        _selectedTableView.table.seats = dict[@"seats"];
        _selectedTableView.table.opposing = dict[@"opposing"];
        [_selectedTableView setNeedsDisplay];
        return [RACSignal empty];
    }];
    
    _tableSettingView.duplicateButton.rac_command = [self duplicateTableToRoomCommand];
    _tableSettingView.removeButton.rac_command = [self removeTableFromRoomCommand];

}

- (void)cancelEditing {
    _selectedTableView.isSelected = NO;
    [_selectedTableView setNeedsDisplay];
    _selectedTableView = nil;
    [self reloadTableSettingView];
}

#pragma mark - UICollectionView Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.viewModel.rooms.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    RoomCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ROOMCELL" forIndexPath:indexPath];
    Room *room = [self.viewModel.rooms objectAtIndex:indexPath.row];
    cell.nameLabel.text = room.name;
    if (cell.selected) {
        cell.selectedMarkView.hidden = NO;
    } else {
        cell.selectedMarkView.hidden = YES;
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    for (UICollectionViewCell *cell in [collectionView visibleCells]) {
        RoomCell *roomCell = (RoomCell *)cell;
        roomCell.selectedMarkView.hidden = YES;
    }
    [self selectRoomAtIndex:indexPath.row];
}


@end
