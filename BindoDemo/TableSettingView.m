//
//  TableSettingView.m
//  BindoDemo
//
//  Created by Matt Chang on 9/19/15.
//  Copyright (c) 2015 Matt Chang. All rights reserved.
//

#import "TableSettingView.h"

@interface TableSettingView () {
    UIButton *_squareButton;
    UILabel *_squareLable;
    UIButton *_roundButton;
    UILabel *_roundLable;
    UILabel *_numberLabel;
    UILabel *_seatsLabel;
    UIButton *_opposingButton;

}

- (void)setup;
- (void)selectTableType:(TableType)type;

@end

@implementation TableSettingView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        self.isEditing = NO;
        [self setup];
    }
    
    return self;
}

- (void)setup {
    _squareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _squareButton.frame = CGRectMake(70, 100, 50, 50);
    _squareButton.layer.borderColor = [[UIColor colorWithWhite:1.0 alpha:0.5] CGColor];
    _squareButton.layer.borderWidth = 1.0;
    [self addSubview:_squareButton];
    
    _squareLable = [[UILabel alloc] initWithFrame:CGRectMake(45, 155, 100, 20)];
    _squareLable.text = @"Square";
    _squareLable.textColor = [UIColor colorWithWhite:1.0 alpha:0.5];
    _squareLable.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_squareLable];
    
    _roundButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _roundButton.frame = CGRectMake(200, 100, 50, 50);
    _roundButton.layer.cornerRadius = 25.0f;
    _roundButton.layer.borderColor = [[UIColor colorWithWhite:1.0 alpha:0.5] CGColor];
    _roundButton.layer.borderWidth = 1.0;
    [self addSubview:_roundButton];
    
    _roundLable = [[UILabel alloc] initWithFrame:CGRectMake(175, 155, 100, 20)];
    _roundLable.text = @"Round";
    _roundLable.textColor = [UIColor colorWithWhite:1.0 alpha:0.5];
    _roundLable.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_roundLable];
    
    self.type = TableTypeSquare;
    [self selectTableType:TableTypeSquare];
    
    UILabel *tableNumberLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 300, 150, 30)];
    tableNumberLabel.text = @"Table Number";
    tableNumberLabel.textColor = [UIColor colorWithWhite:0.7 alpha:1];
    [self addSubview:tableNumberLabel];
    
    UILabel *tableSeatsLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 400, 150, 30)];
    tableSeatsLabel.text = @"Number of Seats";
    tableSeatsLabel.textColor = [UIColor colorWithWhite:0.7 alpha:1];
    [self addSubview:tableSeatsLabel];
    
    UILabel *opposingSeatsLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 500, 150, 30)];
    opposingSeatsLabel.text = @"Opposing Seats";
    opposingSeatsLabel.textColor = [UIColor colorWithWhite:0.7 alpha:1];
    [self addSubview:opposingSeatsLabel];
    
    _numberLabel = [[UILabel alloc] initWithFrame:CGRectMake(200, 300, 30, 30)];
    _numberLabel.textAlignment = NSTextAlignmentCenter;
    _numberLabel.textColor = [UIColor whiteColor];
    _numberLabel.font = [UIFont boldSystemFontOfSize:20];
    _numberLabel.text = @"1";
    [self addSubview:_numberLabel];
    
    _seatsLabel = [[UILabel alloc] initWithFrame:CGRectMake(200, 400, 30, 30)];
    _seatsLabel.textAlignment = NSTextAlignmentCenter;
    _seatsLabel.font = [UIFont boldSystemFontOfSize:20];
    _seatsLabel.textColor = [UIColor whiteColor];
    _seatsLabel.text = @"1";
    [self addSubview:_seatsLabel];
    
    UIButton *minusButton = [UIButton buttonWithType:UIButtonTypeCustom];
    minusButton.frame = CGRectMake(170, 405, 20, 20);
    [minusButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [minusButton setTitle:@"-" forState:UIControlStateNormal];
    [self addSubview:minusButton];
    
    minusButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        NSUInteger number = _seatsLabel.text.integerValue;
        if (number > 1) {
            number--;
            _seatsLabel.text = [NSString stringWithFormat:@"%lu", number];
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"warning" message:@"seats cannot less than 1" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        return [RACSignal empty];
    }];
    
    UIButton *plusButton = [UIButton buttonWithType:UIButtonTypeCustom];
    plusButton.frame = CGRectMake(240, 405, 20, 20);
    [plusButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [plusButton setTitle:@"+" forState:UIControlStateNormal];
    [self addSubview:plusButton];
    
    plusButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        NSUInteger number = _seatsLabel.text.integerValue;
        if (number < 12) {
            number++;
            _seatsLabel.text = [NSString stringWithFormat:@"%lu", number];
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"warning" message:@"seats cannot over than 12" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        return [RACSignal empty];
    }];
    
    _opposingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _opposingButton.frame = CGRectMake(200, 500, 30, 30);
    _opposingButton.layer.cornerRadius = 15.0f;
    _opposingButton.layer.borderColor = [[UIColor whiteColor] CGColor];
    _opposingButton.layer.borderWidth = 0.7;
    [self addSubview:_opposingButton];
    self.isOpposing = NO;
    
    @weakify(self);
    _squareButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        @strongify(self);
        self.type = TableTypeSquare;
        [self selectTableType:TableTypeSquare];
        _opposingButton.enabled = YES;
        _opposingButton.hidden = NO;
        return [RACSignal empty];
    }];
    
    _roundButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        @strongify(self);
        self.type = TableTypeRound;
        [self selectTableType:TableTypeRound];
        _opposingButton.enabled = NO;
        _opposingButton.hidden=  YES;
        _opposingButton.backgroundColor = [UIColor clearColor];
        self.isOpposing = NO;
        return [RACSignal empty];
    }];
    
    _opposingButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        @strongify(self);
        if (!self.isOpposing) {
            _opposingButton.backgroundColor = [UIColor whiteColor];
            self.isOpposing = YES;
        } else {
            _opposingButton.backgroundColor = [UIColor clearColor];
            self.isOpposing = NO;
        }
        return [RACSignal empty];
    }];
    
    self.doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.doneButton.frame = CGRectMake(57.5, CGRectGetHeight(self.frame)-50-64, 100, 37.5);
    self.doneButton.layer.cornerRadius = 5.0f;
    self.doneButton.backgroundColor = [UIColor greenColor];
    [self.doneButton setTitle:@"Done" forState:UIControlStateNormal];
    [self.doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self addSubview:self.doneButton];
    
    self.cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.cancelButton.frame = CGRectMake(170, CGRectGetHeight(self.frame)-50-64, 100, 37.5);
    self.cancelButton.layer.cornerRadius = 5.0f;
    self.cancelButton.backgroundColor = [UIColor redColor];
    [self.cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [self.cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self addSubview:self.cancelButton];
    
    self.reloadButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.reloadButton.frame = CGRectMake(10, CGRectGetHeight(self.frame)-50-64, 37.5, 37.5);
    self.reloadButton.layer.cornerRadius = 5.0f;
    self.reloadButton.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.7];
    [self.reloadButton setTitle:@"R" forState:UIControlStateNormal];
    [self addSubview:self.reloadButton];
    
    self.duplicateButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.duplicateButton.frame = CGRectMake(57.5, CGRectGetHeight(self.frame)-50-64, 100, 37.5);
    self.duplicateButton.layer.cornerRadius = 5.0f;
    self.duplicateButton.backgroundColor = [UIColor colorWithRed:0 green:0.6 blue:0.9 alpha:1];
    [self.duplicateButton setTitle:@"Duplicate" forState:UIControlStateNormal];
    [self.duplicateButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self addSubview:self.duplicateButton];
    
    self.removeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.removeButton.frame = CGRectMake(170, CGRectGetHeight(self.frame)-50-64, 100, 37.5);
    self.removeButton.layer.cornerRadius = 5.0f;
    self.removeButton.backgroundColor = [UIColor redColor];
    [self.removeButton setTitle:@"Remove" forState:UIControlStateNormal];
    [self.removeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self addSubview:self.removeButton];
    
    self.reloadButton.hidden = YES;
    self.duplicateButton.hidden = YES;
    self.removeButton.hidden = YES;
}

- (void)selectTableType:(TableType)type {
    if (type == TableTypeSquare) {
        _squareButton.backgroundColor = [UIColor whiteColor];
        _squareLable.textColor = [UIColor whiteColor];
        _roundButton.backgroundColor = [UIColor clearColor];
        _roundLable.textColor = [UIColor colorWithWhite:1.0 alpha:0.5];
    } else {
        _squareButton.backgroundColor = [UIColor clearColor];
        _squareLable.textColor = [UIColor colorWithWhite:1.0 alpha:0.5];
        _roundButton.backgroundColor = [UIColor whiteColor];
        _roundLable.textColor = [UIColor whiteColor];
    }
}

#pragma mark -Public

- (void)resetSettingView:(NSUInteger)numberIndex {
    self.type = TableTypeSquare;
    _opposingButton.backgroundColor = [UIColor clearColor];
    self.isOpposing = NO;
    _numberLabel.text = [NSString stringWithFormat:@"%lu", numberIndex];
    
    self.doneButton.hidden = NO;
    self.cancelButton.hidden = NO;
    self.reloadButton.hidden = YES;
    self.duplicateButton.hidden = YES;
    self.removeButton.hidden = YES;
}

- (NSDictionary *)getTableInfo {
    return @{
             @"type": @(self.type),
             @"name": _numberLabel.text,
             @"seats": @(_seatsLabel.text.integerValue),
             @"opposing": @(self.isOpposing),
             @"position": NSStringFromCGPoint(CGPointMake(300, 300))
             };
}

- (NSDictionary *)duplicateTable {
    return @{
             @"type": @(self.type),
             @"name": [NSString stringWithFormat:@"%lu", _numberLabel.text.integerValue+1],
             @"seats": @(_seatsLabel.text.integerValue),
             @"opposing": @(self.isOpposing),
             @"position": NSStringFromCGPoint(CGPointMake(300, 300))
             };
}

- (void)changeToEditMode:(NSDictionary *)dict {
    /*
     @{
     @"type": @(self.type),
     @"name": _numberLabel.text,
     @"seats": @(_seatsLabel.text.integerValue),
     @"opposing": @(self.isOpposing),
     }
     */
    self.isEditing = YES;
    self.type = [dict[@"type"] integerValue];
    _numberLabel.text = dict[@"name"];
    _seatsLabel.text = [NSString stringWithFormat:@"%lu", [dict[@"seats"] integerValue]];
    if ([dict[@"opposing"] integerValue] == 0) {
        _opposingButton.backgroundColor = [UIColor clearColor];
        self.isOpposing = NO;
    } else {
        _opposingButton.backgroundColor = [UIColor whiteColor];
        self.isOpposing = YES;
    }
    
    self.doneButton.hidden = YES;
    self.cancelButton.hidden = YES;
    self.reloadButton.hidden = NO;
    self.duplicateButton.hidden = NO;
    self.removeButton.hidden = NO;
    
}

@end
