//
//  RoomListLayout.h
//  BindoDemo
//
//  Created by Matt Chang on 9/17/15.
//  Copyright (c) 2015 Matt Chang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoomListLayout : UICollectionViewFlowLayout

@end
