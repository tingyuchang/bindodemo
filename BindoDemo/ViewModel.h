//
//  ViewModel.h
//  BindoDemo
//
//  Created by Matt Chang on 9/17/15.
//  Copyright (c) 2015 Matt Chang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa.h>
#import <RACEXTScope.h>
#import "Room.h"
#import "Table.h"

@interface ViewModel : NSObject

@property (strong, nonatomic) NSArray *rooms;
@property (strong, nonatomic) Room *currentRoom;
@property (strong, nonatomic) RACSubject *insertTableSubject;

- (RACSignal *)insertRoom:(NSString *)name;
- (RACSignal *)insertTable:(NSDictionary *)dict;
- (RACSignal *)saveSetting;
- (RACSignal *)removeCurrentRoom;
- (RACSignal *)removeTable:(Table *)table;
- (void)selectedCurrentRoom:(NSUInteger)index;

- (void)syncData;

@end
