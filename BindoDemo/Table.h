//
//  Table.h
//  BindoDemo
//
//  Created by Matt Chang on 9/17/15.
//  Copyright (c) 2015 Matt Chang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, TableType) {
    TableTypeSquare = 0,
    TableTypeRound = 1
};

@class Room;

@interface Table : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * opposing;
@property (nonatomic, retain) NSString * position;
@property (nonatomic, retain) NSNumber * seats;
@property (nonatomic, retain) NSNumber * type;
@property (nonatomic, retain) Room *room;

- (TableType)tableType;
- (NSUInteger)count;
- (CGPoint)center;
- (BOOL)isOpposing;

@end
