//
//  Room.m
//  BindoDemo
//
//  Created by Matt Chang on 9/17/15.
//  Copyright (c) 2015 Matt Chang. All rights reserved.
//

#import "Room.h"
#import "Table.h"


@implementation Room

@dynamic name;
@dynamic tables;

@end
