//
//  TableView.h
//  MultipeerDemo
//
//  Created by Matt Chang on 9/11/15.
//  Copyright (c) 2015 Accuvally Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Table.h"

@interface TableView : UIView

@property (strong, nonatomic) Table *table;
@property (assign) BOOL isSelected;
@property (strong, nonatomic) void(^tapHanlder)();

- (id)initWithTable:(Table *)table;

@end
