//
//  ConnectionManager.m
//  BindoDemo
//
//  Created by Matt Chang on 9/20/15.
//  Copyright (c) 2015 Matt Chang. All rights reserved.
//

#import "ConnectionManager.h"

@implementation ConnectionManager


+ (ConnectionManager *)sharedInstance {
    static ConnectionManager *_sharedInstance = nil;
    
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[ConnectionManager alloc] init];
    });
    
    return _sharedInstance;
}

- (id)init {
    self = [super init];
    
    if (self) {
        _peerId = nil;
        _session = nil;
        _browser = nil;
        _advertiser = nil;
    }
    
    return self;
}

#pragma mark - Public

- (void)setupPeerAndSessionWithDisplayName:(NSString *)displayName {
    self.peerId = [[MCPeerID alloc] initWithDisplayName:displayName];
    
    self.session = [[MCSession alloc] initWithPeer:self.peerId];
    self.session.delegate = self;
}

- (void)setupBrowser {
    self.browser = [[MCBrowserViewController alloc] initWithServiceType:@"BindoDemo" session:_session];
}

- (void)advertiseSelf:(BOOL)isAdvertising {
    if (isAdvertising) {
        self.advertiser = [[MCAdvertiserAssistant alloc] initWithServiceType:@"BindoDemo" discoveryInfo:nil session:self.session];
        [self.advertiser start];
    } else {
        [self.advertiser stop];
        self.advertiser = nil;
    }
}

#pragma mark - MCSessionDelegate

// Remote peer changed state
- (void)session:(MCSession *)session peer:(MCPeerID *)peerID didChangeState:(MCSessionState)state {
    /*
     MCSessionStateNotConnected,     // not in the session
     MCSessionStateConnecting,       // connecting to this peer
     MCSessionStateConnected
     */
    NSDictionary *dict = @{
                           @"peerID": peerID,
                           @"state" : [NSNumber numberWithInt:state]
                           };
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MCDidChangeStateNotification"
                                                        object:nil
                                                      userInfo:dict];
}

- (void)session:(MCSession *)session didReceiveData:(NSData *)data fromPeer:(MCPeerID *)peerID {
    NSLog(@"Data: %@", [NSKeyedUnarchiver unarchiveObjectWithData:data]);
    
    // when Received Data check saveRecord if no problem, overwrite DB
    
}

- (void)session:(MCSession *)session didReceiveStream:(NSInputStream *)stream withName:(NSString *)streamName fromPeer:(MCPeerID *)peerID {
    
}

- (void)session:(MCSession *)session didStartReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID withProgress:(NSProgress *)progress {
    
}

- (void)session:(MCSession *)session didFinishReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID atURL:(NSURL *)localURL withError:(NSError *)error {
    
}

@end
