//
//  ConnectionManager.h
//  BindoDemo
//
//  Created by Matt Chang on 9/20/15.
//  Copyright (c) 2015 Matt Chang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MultipeerConnectivity/MultipeerConnectivity.h>

@interface ConnectionManager : NSObject <MCSessionDelegate>

@property (strong, nonatomic) MCPeerID *peerId;
@property (strong, nonatomic) MCSession *session;
@property (strong, nonatomic) MCBrowserViewController *browser;
@property (strong, nonatomic) MCAdvertiserAssistant *advertiser;

+ (ConnectionManager *)sharedInstance;

- (void)setupPeerAndSessionWithDisplayName:(NSString *)displayName;
- (void)setupBrowser;
- (void)advertiseSelf:(BOOL)isAdvertising;

@end
